class RemovePasswordFromAdmin < ActiveRecord::Migration
    def up
        remove_column :administrators, :password
    end
    def down
        add_column :administrators, :password, :string
    end
end
