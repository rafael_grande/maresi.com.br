class Administrator < ActiveRecord::Base
  validates_presence_of :email, :name, :password
  validates_confirmation_of :password
  has_secure_password
    
    
    def self.authenticate(email, password)
            find_by_email(email).
            try(:authenticate, password)
    end
end
